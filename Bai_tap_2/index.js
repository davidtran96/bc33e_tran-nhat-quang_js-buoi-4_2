function tinhNgay() {
  var m = document.getElementById("txt-thang").value * 1,
    y = document.getElementById("txt-nam").value * 1,
    a = "";
  switch (m) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      a = 31;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      a = 30;
      break;
    case 2:
      if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
        a = 29;
        break;
      } else {
        a = 29;
        break;
      }
  }
  document.getElementById(
    "result"
  ).innerHTML = ` Tháng ${m} năm ${y} có ${a} ngày`;
}
