function docSo() {
  var e = document.getElementById("txt-number").value * 1,
    t = Math.floor(e / 100),
    c = Math.floor((e % 100) / 10),
    a = "";
  switch (t) {
    case 0:
      a += "";
      break;
    case 1:
      a += "Một trăm ";
      break;
    case 2:
      a += "Hai trăm ";
      break;
    case 3:
      a += "Ba trăm ";
      break;
    case 4:
      a += "Bốn trăm ";
      break;
    case 5:
      a += "Năm trăm ";
      break;
    case 6:
      a += "Sáu trăm ";
      break;
    case 7:
      a += "Bảy trăm ";
      break;
    case 8:
      a += "Tám trăm ";
      break;
    case 9:
      a += "Chín trăm ";
      break;
  }
  switch (c) {
    case 0:
      a += "lẻ ";
      break;
    case 1:
      a += "mười ";
      break;
    case 2:
      a += "hai mươi ";
      break;
    case 3:
      a += "ba mươi ";
      break;
    case 4:
      a += "bốn mươi ";
      break;
    case 5:
      a += "năm mươi ";
      break;
    case 6:
      a += "sáu mươi ";
      break;
    case 7:
      a += "bảy mươi ";
      break;
    case 8:
      a += "tám mươi ";
      break;
    case 9:
      a += "chín mươi ";
      break;
  }
  switch ((e % 100) % 10) {
    case 0:
      a += "";
      break;
    case 1:
      a += "mốt";
      break;
    case 2:
      a += "hai";
      break;
    case 3:
      a += "ba";
      break;
    case 4:
      a += "bốn";
      break;
    case 5:
      a += "năm";
      break;
    case 6:
      a += "sáu";
      break;
    case 7:
      a += "bảy";
      break;
    case 8:
      a += "tám";
      break;
    case 9:
      a += "chín";
      break;
  }
  document.getElementById("result").innerHTML = a;
}
