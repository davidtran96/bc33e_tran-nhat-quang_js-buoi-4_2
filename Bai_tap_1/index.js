function dateTomorow() {
  var d = document.getElementById("txt-ngay").value * 1,
    m = document.getElementById("txt-thang").value * 1,
    y = document.getElementById("txt-nam").value * 1;
  a = "";
  switch (m) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
      if (d > 0 && d < 31) {
        a = d + 1 + "/" + m + "/" + y;
        break;
      } else if (d == 31) {
        a = "1/" + (m + 1) + "/" + y;
        break;
      }
    case 2:
      if (d > 0 && d < 28) {
        a = d + 1 + "/" + m + "/" + y;
        break;
      } else if (d == 28) {
        a = "1/" + (m + 1) + "/" + y;
        break;
      }
    case 4:
    case 6:
    case 9:
    case 11:
      if (d > 0 && d < 30) {
        a = d + 1 + "/" + m + "/" + y;
        break;
      } else if (d == 30) {
        a = "1/" + (m + 1) + "/" + y;
        break;
      }
    case 12:
      if (d > 0 && d < 31) {
        a = d + 1 + "/" + m + "/" + y;
        break;
      } else if (d == 31) {
        a = "1/1/" + (y + 1);
        break;
      }
    default:
      a = "Tháng không xác định";
  }
  document.getElementById("result").innerHTML = a;
}
function dateYesterday() {
  var d = document.getElementById("txt-ngay").value * 1,
    m = document.getElementById("txt-thang").value * 1,
    y = document.getElementById("txt-nam").value * 1;
  a = "";
  switch (m) {
    case 1:
      if (d > 1 && d < 31) {
        a = d - 1 + "/" + m + "/" + y;
        break;
      } else if (d == 1) {
        a = "31/12/" + (y - 1);
        break;
      }
    case 2:
      if (d > 1 && d < 28) {
        a = d - 1 + "/" + m + "/" + y;
        break;
      } else if (d == 1) {
        a = "31/" + (m - 1) + "/" + y;
        break;
      }
    case 3:
      if (d > 1 && d < 31) {
        a = d - 1 + "/" + m + "/" + y;
        break;
      } else if (d == 1) {
        a = "28/" + (m - 1) + "/" + y;
        break;
      }
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (d > 1 && d < 31) {
        a = d - 1 + "/" + m + "/" + y;
        break;
      } else if (d == 1) {
        a = "30/" + (m - 1) + "/" + y;
        break;
      }
    case 4:
    case 6:
    case 9:
    case 11: {
      if (d > 1 && d < 30) {
        a = d - 1 + "/" + m + "/" + y;
        break;
      } else if (d == 1) {
        a = "31/" + (m - 1) + "/" + y;
        break;
      }
    }
  }
  document.getElementById("result").innerHTML = a;
}
